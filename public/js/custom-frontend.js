jQuery(document).ready(function ($) {
$.ajaxSetup({

    headers:{
        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
    }

});
$(document).on('click','.btn-report-file',function (event) {

    event.preventDefault();
    var $this = $(this);
    var file_id = $this.data('fid');
    $.ajax({
        url:'/files/report',
        type:'post',
        dataType : 'json',
        data:{
            file_id:file_id
        },
        success:function (response) {
            if(response.success){
                swal({
                   text:response.message,
                    title:"خطا در دانلود فایل",
                    icon:"success"
                });
            }
        },
        error:function () {

        }
    })

})

});