<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Frontend\HomeController@index')->name('home');

Route::group(['namespace'=>'Frontend'],function (){

    //accounts

    Route::get('account/login','UserController@login')->name('login');
    Route::post('account/login','UserController@doLogin')->name('doLogin');
    Route::get('account/register','UserController@register')->name('register');
    Route::post('account/register','UserController@doRegister')->name('doRegister');
    Route::get('account/logout','UserController@doLogout')->name('doLogout');

    //user-dashboard
    Route::get('dashboard','DashboardController@index')->name('user.dashboard');

    //subscribe for plan and files
    Route::get('/plans','PlansController@index')->name('frontend.plans.index');
    Route::get('/subscribe/{plan_id}','SubscribeController@index')->name('frontend.subscribe.index');
    Route::post('/subscribe/{plan_id}','SubscribeController@register')->name('frontend.subscribe.register');
    //files
    Route::get('/files/{file_id}','FilesController@details')->name('frontend.files.details');
    Route::get('/files/download/{file_id}','FilesController@download')->name('frontend.files.download');
    Route::post('/files/report','FilesController@report')->name('frontend.files.report');
    Route::get('/access','FilesController@access')->name('frontend.files.access');

    //packages
    Route::get('/packages/{pack_id}','PackagesController@details')->name('frontend.packages.details');
});
//
Route::group(['prefix'=>'admin','namespace'=>'Admin','middleware'=>'admin'],function (){

    //users routes
    Route::get('/','UserController@index')->name('admin.users.list');
    Route::get('/users','UserController@index')->name('admin.users.list');
    Route::get('/users/create','UserController@create')->name('admin.users.create');
    Route::post('/users/create','UserController@store')->name('admin.users.store');
    Route::get('/users/delete/{user_id}','UserController@delete')->name('admin.users.delete');
    Route::get('/users/edit/{user_id}','UserController@edit')->name('admin.users.edit');
    Route::post('/users/edit/{user_id}','UserController@update')->name('admin.users.update');
    Route::get('/users/package/{user_id}','UserController@package')->name('admin.users.package');
    // files routes
    Route::get('/files','FilesController@index')->name('admin.files.index');
    Route::get('/files/create','FilesController@create')->name('admin.files.create');
    Route::post('/files/create','FilesController@store')->name('admin.files.store');
    Route::get('/files/edit/{file_id}','FilesController@edit')->name('admin.files.edit');
    Route::post('/files/edit/{file_id}','FilesController@update')->name('admin.files.update');

    // plans routes

    Route::get('/plans','PlansController@index')->name('admin.plans.index');
    Route::get('/plans/create','PlansController@create')->name('admin.plans.create');
    Route::post('/plans/create','PlansController@store')->name('admin.plans.store');
    Route::get('/plans/edit/{plan_id}','PlansController@edit')->name('admin.plans.edit');
    Route::post('/plans/edit/{plan_id}','PlansController@update')->name('admin.plans.update');
    Route::get('/plans/delete/{plan_id}','PlansController@delete')->name('admin.plans.delete');


    // packages routes

    Route::get('/packages','PackagesController@index')->name('admin.packages.index');
    Route::get('/packages/create','PackagesController@create')->name('admin.packages.create');
    Route::post('/packages/create','PackagesController@store')->name('admin.packages.store');
    Route::get('/packages/edit/{package_id}','PackagesController@edit')->name('admin.packages.edit');
    Route::post('/packages/edit/{package_id}','PackagesController@update')->name('admin.packages.update');
    Route::get('/packages/delete/{package_id}','PackagesController@delete')->name('admin.packages.delete');
    Route::get('/packages/syncs_files/{package_id}','PackagesController@syncFiles')->name('admin.packages.syncs_file');
    Route::post('/packages/syncs_files/{package_id}','PackagesController@uploadSyncFiles')->name('admin.packages.syncs_file');

    // payments routes

    Route::get('/payments','PaymentsController@index')->name('admin.payments.index');
//    Route::get('/payments/create','PaymentsController@create')->name('admin.payments.create');
//    Route::post('/payments/create','PaymentsController@store')->name('admin.payments.store');
//    Route::get('/payments/edit/{payment_id}','PaymentsController@edit')->name('admin.payments.edit');
//    Route::post('/payments/edit/{payment_id}','PaymentsController@update')->name('admin.payments.update');
   Route::get('/payments/delete/{payment_id}','PaymentsController@delete')->name('admin.payments.delete');

    // categories routes

    Route::get('/categories','CategoriesController@index')->name('admin.categories.index');
    Route::get('/categories/create','CategoriesController@create')->name('admin.categories.create');
    Route::post('/categories/create','CategoriesController@store')->name('admin.categories.store');
    Route::get('/categories/edit/{package_id}','CategoriesController@edit')->name('admin.categories.edit');
    Route::post('/categories/edit/{package_id}','CategoriesController@update')->name('admin.categories.update');
    Route::get('/categories/delete/{package_id}','CategoriesController@delete')->name('admin.categories.delete');


});


