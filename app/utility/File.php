<?php
/**
 * Created by PhpStorm.
 * User: Hormati
 * Date: 10/30/2018
 * Time: 2:11 PM
 */

namespace App\utility;


use App\Models\Subscribe;
use App\User;

class File
{
    public static function user_can_download(int $user_id)
    {
        if (!\App\utility\User::is_user_subscribed($user_id)) {
            return false;
        }
        $userItem = User::find($user_id);
        $user_subscribe = $userItem->currentSubscribe()->first();
        if (!$user_subscribe) {
            return false;
        }
        if ($user_subscribe->subscribe_download_limit <= $user_subscribe->subscribe_download_count) {
            return false;
        }
        return true;
    }
}