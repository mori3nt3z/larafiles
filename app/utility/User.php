<?php
/**
 * Created by PhpStorm.
 * User: Hormati
 * Date: 10/29/2018
 * Time: 9:59 AM
 */

namespace App\utility;


use App\Models\Subscribe;
use Carbon\Carbon;
use PhpParser\Node\Expr\Instanceof_;

class User
{

    public static function is_user_subscribed(int $user_id = null):bool
    {

        $user= \App\User::find($user_id);
        if(!$user){
            return false;
        }

        $user_subscribe = $user->currentSubscribe()->first();
        return !empty($user_subscribe) ;
    }
    
}