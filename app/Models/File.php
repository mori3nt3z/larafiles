<?php

namespace App\Models;

use App\Traits\Categorizable;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
//    protected $table='files';
use Categorizable;
    protected $primaryKey = 'file_id';

    protected $guarded = ['file_id'];


    public function packages()
    {
        return $this->belongsToMany(Package::class,'package_file','file_id','package_id');
    }


    public function getFileTypeTextAttribute()
    {
        $type =[
          'image/png'=>'PNG',
            'application/pdf'=>'PDF',
        ];
       // thats is a meditend
        return $type[$this->attributes['file_type']];
    }
}
