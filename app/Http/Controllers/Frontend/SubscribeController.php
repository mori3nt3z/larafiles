<?php

namespace App\Http\Controllers\Frontend;

use App\Mail\UserSubscribed;
use App\Models\Plan;
use App\Models\Subscribe;
use App\utility\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SubscribeController extends Controller
{
    public function index(Request $request,int $plan_id)
    {

        $plan_item =Plan::find($plan_id);
        return view('frontend.subscribe.index',compact('plan_item'));
    }

    /**
     * @param Request $request
     * @param int $plan_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register(Request $request, int $plan_id)
    {

       $plan = Plan::find($plan_id);
        if(!$plan){
            return redirect()->back()->with(['hasError' => 'طرح مورد نظر شما معتبر نمی باشد ']);
        }

        $plans_days_count =$plan->plan_days_count;
        $expired_at = Carbon::now()->addDays( $plans_days_count);

        $user=Auth::user();

        $subscribeData = [
            'subscribe_user_id' =>$user->id,
            'subscribe_plan_id' => $plan_id,
            'subscribe_download_limit' => $plan->plan_limit_download_count,
            'subscribe_created_at' => Carbon::now(),
            'subscribe_expire_at' => $expired_at,
        ];



        Subscribe::create($subscribeData);
     //Mail::to($user)->later(Carbon::now()->addMinutes(15),new UserSubscribed($subscribe));

    }
}
