<?php

namespace App\Http\Controllers\Frontend;

use App\Models\File;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FilesController extends Controller
{

    public function details(Request $request, int $file_id)
    {

        $fileItem = File::find($file_id);

        $current_user = Auth::user()->id;

        return view('frontend.files.single',compact('fileItem','current_user'));
    }

    public function download(Request $request,int $file_id)
    {


        $currentUser = Auth::user();
        if(!\App\utility\File::user_can_download($currentUser->id)){
            return redirect()->route('frontend.files.access');
        }
        $fileItem = File::find($file_id);
        if(!$fileItem){

            redirect()->back()->with('fileError','فایل دخواستی موجود نمی باشد ');
        }


//        dd($fileDownload);
        $BasePath = public_path('images\\');
        $filePath = $fileItem->file_name;
        $fileDownload = $BasePath.$filePath;

// increment current user subscribed ... limit download
        $currentUser->currentSubscribe()->first()->increment('subscribe_download_count');

        return response()->download($fileDownload);
    }

    public function access()
    {
        return view('frontend.files.access');
    }

    public function report(Request $request)
    {


        $file_id = $request->input('file_id');
        if($file_id && intval($file_id) >0){

            $fileItem = File::find($file_id);
            $fileItem->increment('file_report_count');

            return [
                'success'=> true,
                'message' => 'درخواست شما ثبت و بعد از 24 ساعت به پیگیری شما پاسخ داده می شود'
            ];

        }
        return [
          'success' => false,
            'message' => 'درخواست شما معتبر نمی باشد '
        ];
    }
}
