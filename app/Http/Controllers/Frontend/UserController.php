<?php

namespace App\Http\Controllers\Frontend;

use App\Events\UserRegistered;
use App\Jobs\EmailSendNotify;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login()
    {
        return view('frontend.users.login');
    }

    public function register()
    {
        return view('frontend.users.register');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function doLogin(Request $request)
    {

        $remember = $request->has('remember');

        $this->validate($request,[
            'email'=> 'required',
            'password'=> 'required'
        ],[
            'email.required' => 'وارد کردن ایمیل الزامی است',
            'password.required'=>'کلمه عبور وارد نشده است '

        ]);

        //if(Auth::attempt(['email' =>$request->input('email'),'password'=>$request->input('password')])){

        if(Auth::attempt(['email' =>$request->input('email'),'password'=>$request->input('password')],$remember)){

//            $user = Auth::user();
//            $job = (new EmailSendNotify($user))->onQueue('email');
//            $job->delay(Carbon::now()->addMinutes(10));
//            dispatch($job);
            return redirect()->route('home')->with(['loginSuccess' => 'شما با موفقیت وارد شدید']);
        }else{
            return redirect()->back()->with(['loginError'=>'ایمیل یا رمز عبور نامعتبر است ']);
        }
    }

    public function doRegister(Request $request)
    {

        //request validation
        $this->validate($request,[

            'fullName' => 'required|min:5',
            'email' => 'required',
            'password' => 'required|min:8'
        ],[
            'fullName.required' =>'فیلد نام خالی است ',
            'fullName.min' =>'فیلد نام حداقل پنج کاراکتر است ',
            'email.required' =>'وارد کردن ایمیل  الزامی است ',
            'password.required' =>'کلمه عبور را وارد کنید',
            'password.min' =>'کلمه عبور حداقل هشت کاراکتر است ',

        ]);
        //get new user data from request
        $newUserData = [
            'name' => $request->input('fullName'),
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];
        // create new user with data in database
        $newUser = User::create($newUserData);
        // checking user is valid  or not
        // user valid
        if($newUser && $newUser instanceof User){

            event(new UserRegistered($newUser));

            return redirect()->route('login')->with(['registerSuccess' => 'شما با موفقیت ثبت نام کردید']);
        }
        //  user is not valid
        return redirect()->back()->with(['registerError' => 'ثبت نام شما با مشکل روبرو شد لطفا 5 دقیقه دیگر امتحان فرمایید ']);


    }

    public function doLogout()
    {
        Auth::logout();
        return redirect()->route('home')->with(['logoutSuccess' => 'شما با موفقیت خارج شدید']);
    }
}
