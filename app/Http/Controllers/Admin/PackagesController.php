<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\File;
use App\Models\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackagesController extends Controller
{
    public function index()
    {
        $packages = Package::all();

        return view('admin.package.index',compact('packages'))->with(['panel_title' =>'لیست پکیج ها' ]);

    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.package.create',compact('categories'))->with(['panel_title' =>'ثبت پکیج جدید']);
    }

    public function store(Request $request)
    {

        //validation
        $this->validate($request,[
            'package_title' => 'required'


        ],[
            'package_title.required' => 'عنوان پکیج الزامیست'
        ]);

        $newPackageInputs = [

            'package_title' => $request->input('package_title'),

            'package_price' => $request ->input('package_price'),

        ];

        $newPackageCreated = Package::create($newPackageInputs);

        if($newPackageCreated){
            if($request->has('categorize')){
                $newPackageCreated->categories()->sync($request->input('categorize'));
            }
            return redirect()->route('admin.packages.index')->with(['success' =>'پکیج جدید با موفقیت ثبت شد']);
        }


    }

    public function edit(Request $request,$package_id)
    {


        $package_id = intval($package_id);

        $packageItem = Package::find($package_id);

        $categories = Category::all();
        $package_categories = $packageItem->categories()->get()->pluck('category_id')->toArray();

        return view('admin.package.edit',compact('packageItem','categories','package_categories'))->with(['panel_title' => 'فرم ویرایش پکیج ها']);
    }

    public function update(Request $request,$package_id)
    {  //validation
        $this->validate($request,[
            'package_title' => 'required'


        ],[
            'package_title.required' => 'عنوان پکیج الزامیست'
        ]);

        $package_id = intval($package_id);

        $packageItem = Package::find($package_id);

        $newpackageItem = [

            'package_title' => $request->input('package_title'),

            'package_price' => $request ->input('package_price'),



        ];


        if($packageItem){
            $packageItem -> update($newpackageItem);
            if($request->has('categorize')){
                $packageItem->categories()->sync($request->input('categorize'));
            }

            return redirect()->route('admin.packages.index')->with(['success' => ' پک مورد نظر ویرایش شد.']);
        }

    }

    public function delete(Request $request,$package_id)
    {

        $package_id=intval($package_id);

        $packageItem = Package::destroy($package_id);
        if($packageItem){

            return redirect()->route('admin.packages.index')->with(['success' => 'پکیج مورد نظر حذف گردید']);
        }
    }

    public function syncFiles(Request $request,$package_id)
    {
        $files = File::all();
        $package_item = Package::find($package_id);
        $package_files= $package_item->files()->get()->pluck('file_id')->toArray();


//        $files_ids =[];
//        foreach($package_files as $package_file ){
//
//            $files_ids[] = $package_file -> file_id;
//
//        }


        return view('admin.package.file',compact('files','package_files'))->with(['panel_title' => 'انتخاب فایلهای مورد نیاز برای این پکیج']);
    }

    public function uploadSyncFiles(Request $request,$package_id)
    {
        $package_item = Package::find($package_id);
        $files = $request->input('files');

        if($package_item && is_array($files) ){

            $package_item->files()->sync($files);
            return redirect()->route('admin.packages.index')->with(['success' => 'فایلها با موفقیت ثبت شد ']);
        }


    }
}
