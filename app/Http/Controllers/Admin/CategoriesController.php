<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories= Category::all();
        return view('admin.category.index',compact('categories'))->with(['panel_title' => 'لیست دسته بندی ها']);
    }

    public function create(){

        return view('admin.category.create')->with(['panel_title' =>' ایجاد دسته بندی جدید ']);

    }

    public function store(Request $request)
    {

        //validation
         $this->validate($request,[
           'category_name'=>'required'
        ],[
            'category_name.required' =>'نوشتن عنوان دسته بندی الزامی است '
        ]);

        $new_category = Category::create([

         'category_name'=>$request->input('category_name')

        ]);
        if($new_category){
            return redirect()->route('admin.categories.index')->with(['success' => 'دسته بندی جدید با موفقیت ثبت شد ']);
        }



    }
    public function edit(Request $request,$category_id){

$category_id = intval($category_id);
        $categoryItem = Category::find($category_id);
        if($categoryItem){

            return view('admin.category.edit',compact('categoryItem'))->with(['panel_title' => 'ویرایش دسته بندی ']);
        }

    }


    public function update(Request $request,$category_id)
    {
        $this->validate($request,[
            'category_name'=>'required'
        ],[
            'category_name.required' =>'نوشتن عنوان دسته بندی الزامی است '
        ]);
        $category_id = intval($category_id);
        $categoryItem = Category::find($category_id);
        $categoryNewItem =[


            'category_name' => $request->input('category_name')
        ];

        $categoryItem ->update($categoryNewItem);

        if($categoryItem){

            return redirect()->route('admin.categories.index')->with(['success' =>'ویرایش دسته بندی با موفقیت انجام شد']);
        }

    }

    public function delete(Request $request,$category_id)
    {

        $category_id =intval($category_id);
        $categoryItem = Category::destroy($category_id);

        if($categoryItem){
            return redirect()->route('admin.categories.index')->with(['success' => 'حذف دسته بندی مورد نظر با موفقیت انجام شد ']);
        }

    }

}
