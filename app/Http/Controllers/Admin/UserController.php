<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
//        $user = User::find(2);
//        $user->packages()->sync([2 =>['amount' => 12000 , 'created_at' => date('y-m-d H:i:s') ]]);
        $users = User::all();

        return view('admin.user.index', compact('users'))->with(['panel_title' => 'لیست کاربران']);

    }

    public function create()
    {

        return view('admin.user.create')->with(['panel_title' => 'افزودن کاربر جدید']);
    }

    public function store(UserRequest $userRequest)
    {


        $user_data = [
            'name' => request()->input('name'),
            'email' => request()->input('email'),
            'password' => request()->input('password'),

            'wallet' => request()->input('wallet'),
            'role' => request()->input('role'),

        ];

        $new_user_created = User::create($user_data);

        if ($new_user_created instanceof User) {

            return redirect()->route('admin.users.list')->with('success', 'کاربر جدید با موفقیت ثبت گردید');
        }

    }

    public function delete($user_id)
    {
        User::destroy($user_id);
        return redirect()->route('admin.users.list')->with('success', 'کاربر مورد نظر با موفقیت حذف گردید');
    }

    public function edit($user_id)
    {

        if ($user_id && ctype_digit($user_id)) {

            $userItem = User::find($user_id);
            if ($userItem && $userItem instanceof User) {
                return view('admin.user.edit',compact('userItem'))->with(['panel_title' => 'ویرایش کاربر']);
            }
        }
    }

    public function update(UserRequest $userRequest,$user_id)
    {


        $inputs = [
            'name' => request()->input('name'),
            'email' => request()->input('email'),
            'password' => request()->input('password'),

            'wallet' => request()->input('wallet'),
            'role' => request()->input('role'),
        ];


        if(!request()->has('password')){
            //empty(request()->input('password')
            unset($inputs['password']);
        }
//        $inputs['password'] = bcrypt($inputs['password']);

        $userItem = User::find($user_id);
        $userItem ->update($inputs);
        return redirect()->route('admin.users.list')->with('success', 'ویرایش کاربر  با موفقیت ثبت گردید');

    }

    public function package(Request $request,$user_id)
    {

        $user = User::find($user_id);
        if(!$user){
            return redirect()->back();
        }

        $user_package=$user->packages()->get();

//        dd($user_package);

        return view('admin.user.package',compact('user_package'))->with(['panel_title' => 'پکیج های خریداری شده کاربر']);
    }
}
