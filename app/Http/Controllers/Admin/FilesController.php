<?php

namespace App\Http\Controllers\Admin;

use App\Models\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilesController extends Controller
{
    public function index()
    {
        $files =File::all();
        return view('admin.file.index',compact('files'))->with(['panel_title' => 'لیست فایل ها ']);
    }//

    public function create()
    {
        return view('admin.file.create') ->with(['panel_title' => 'فرم ایجاد فایل جدید ']);
    }


    public function store(Request $request)
    {

        $this->validate($request,[
            'file_title' => 'required',
            'file_item' => 'required'
        ],[
            'file_title.required'=> 'وارد کردن نام فایل الزامی است ',
            'file_item.required'=> 'هیچ فایلی آپلود نشده است !!'
        ]);

        $newFileCreated = [
            'file_title' => $request->input('file_title'),
            'file_description'=> $request->input('file_description'),
            'file_type' => $request->file('file_item')->getMimeType(),
            'file_size' => $request->file('file_item')->getClientSize(),


        ];

        $newFileName = str_random(10).'.'.$request->file('file_item')->getClientOriginalExtension();
        $result=$request->file('file_item')->move(public_path('images'),$newFileName);

        if($result instanceof \Symfony\Component\HttpFoundation\File\File ){

            $newFileCreated['file_name'] = $newFileName;
            File::create($newFileCreated);
            return redirect()->route('admin.files.index')->with(['success' => 'فایل با موفقیت ثبت شد ']);
        }

    }

    public function edit($file_id)
    {
        if ($file_id && ctype_digit($file_id)) {

            $fileItem = File::find($file_id);
            if ($fileItem && $fileItem instanceof File) {
                return view('admin.file.edit',compact('fileItem'))->with(['panel_title' => 'ویرایش فرم ها']);
            }
        }
    }


    public function update(Request $request,$file_id)
    {
        $inputs=[
            'file_title' => $request->input('file_title'),
            'file_description'=> $request->input('file_description'),
            'file_type' => $request->file('file_item')->getMimeType(),
            'file_size' => $request->file('file_item')->getClientSize(),


        ];

        $fileItem = File::find($file_id);
        $newFileName = str_random(10).'.'.$request->file('file_item')->getClientOriginalExtension();
        $result=$request->file('file_item')->move(public_path('images'),$newFileName);
        if ($result instanceof  \Symfony\Component\HttpFoundation\File\File){

            $fileItem ->update($inputs);
            return redirect()->route('admin.files.index')->with('success', 'ویرایش فرم  با موفقیت ثبت گردید');
        }




    }

}
