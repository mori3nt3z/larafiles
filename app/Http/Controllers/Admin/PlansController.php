<?php

namespace App\Http\Controllers\Admin;

use App\Models\Plan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlansController extends Controller
{
    public function index()
    {
        $plans = Plan::all();
        return view('admin.plan.index',compact('plans'))->with(['panel_title' => 'لیست طرح حهای اشتراکی']);

  }

    public function create()
    {

        return view('admin.plan.create')->with(['panel_title' => 'ایجاد طرح جدید']);
  }

    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'plan_title' => 'required|string',
            'plan_limit_download_count' => 'required|integer',
            'plan_price' => 'required|integer',
            'plan_days_count' => 'required|integer',
        ],[
            'plan_title.required' =>'ثبت عنوان طرح الزامی است ',
            'plan_title.string' =>'عنوان طرح نمی تواند مقدار عددی باشد',
            'plan_limit_download_count.required' =>'ثبت محدودیت روزانه دانلود الزامیست ',
            'plan_limit_download_count.integer' =>'محدودیت روزانه دانلود باید مقدار عددی باشد ',
            'plan_price.required' => 'ثبت ارزش و قیمت طرح الزامی است.',
            'plan_price.integer' => 'ثبت ارزش و قیمت طرح مقدار عددی  است.',
            'plan_days_count.required' => 'قیمت طرح باید به صورت عددی باشد',
            'plan_days_count.integer' => 'محدودیت اعتبار روزانه طرح باید به صورت عددی باشد',
        ]);

        $newPlanInputs = [

            'plan_title' => $request->input('plan_title'),
            'plan_limit_download_count' => $request->input('plan_limit_download_count'),
            'plan_price' => $request ->input('plan_price'),
            'plan_days_count' => $request -> input('plan_days_count'),
        ];

        $newPlanCreated=Plan::create($newPlanInputs);

        if($newPlanCreated instanceof Plan){

           return redirect()->route('admin.plans.index')->with(['success' => 'طرح جدید با موفیت ثبت شد ']);
        }

    }

    public function edit(Request $request,$plan_id)
    {
       $plan_id = intval($plan_id);
        $planItem = Plan::find($plan_id);

        return view('admin.plan.edit', compact('planItem'));

    }
    public function update(Request $request,$plan_id)
    {

        $this->validate($request,[

            'plan_title' => 'required|string',
            'plan_limit_download_count' => 'required|integer',
            'plan_price' => 'required|integer',
            'plan_days_count' => 'required|integer',
        ],[
            'plan_title.required' =>'ثبت عنوان طرح الزامی است ',
            'plan_title.string' =>'عنوان طرح نمی تواند مقدار عددی باشد',
            'plan_limit_download_count.required' =>'ثبت محدودیت روزانه دانلود الزامیست ',
            'plan_limit_download_count.integer' =>'محدودیت روزانه دانلود باید مقدار عددی باشد ',
            'plan_price.required' => 'ثبت ارزش و قیمت طرح الزامی است.',
            'plan_price.integer' => 'ثبت ارزش و قیمت طرح مقدار عددی  است.',
            'plan_days_count.required' => 'قیمت طرح باید به صورت عددی باشد',
            'plan_days_count.integer' => 'محدودیت اعتبار روزانه طرح باید به صورت عددی باشد',
        ]);
        $plan_id = intval($plan_id);
        $planItem = Plan::find($plan_id);

        $newplanItem = [

            'plan_title' => $request->input('plan_title'),
            'plan_limit_download_count' => $request->input('plan_limit_download_count'),
            'plan_price' => $request ->input('plan_price'),
            'plan_days_count' => $request -> input('plan_days_count'),


        ];
        $planItem->update($newplanItem);

        if($planItem instanceof Plan){
           return redirect()->route('admin.plans.index')->with(['success' => 'طرح مورد نظر ویرایش شد']);
        }

    }

    public function delete(Request $request,$plan_id)
    {
        $planItem = Plan::find($plan_id);

        //$planItem = Plan::destroy($plan_id);
        if($planItem){

            $planItem->delete();


        }

        return redirect()->route('admin.plans.index')->with(['success' => 'طرح مورد نظر با موفقیت پاک شد']);
    }
}
