<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [

            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:5|max:10'
        ];

        if (request()->route('user_id') and intval(request()->route('user_id')) > 0) {

            unset ($rules['password']);
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'فیلد نام و نام خانوادگی را کامل کنید با حروف فارسی',
            'email.required' => 'ایمیل خود را وارد کنید',

            'email.email' => 'ایمیل خود را وارد کنید',
            'password.required' => 'ایجاد پسورد الزامی است',
            'password.min' => 'حداقل کلمه عبور 6 کاراکتر است',
            'password.max' => 'حداکثر کلمه عبور 10 کاراکتر است',
        ];
    }
}
