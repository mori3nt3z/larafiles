@extends('layouts.frontend')
@section('content')
    <div class="col-xs-12 col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">طرح های اشتراکی </h3>
            </div>
            <div class="panel-body">

                <form action="" method="post">

                    {{csrf_field()}}
                    <div class="form-group">

                        <label for="plan" ><h4>عنوان طرح</h4></label>
                        <style>
                            table{
                                background-color: rgb(44, 44, 44);

                            }

                            .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th,.table-bordered>thead>tr>th{
                                border: 1px solid #0c0c0c;
                                text-align: center;
                            }

                            table tr th {
                                color: rgb(143, 154, 232);

                            }
                            table tr td{
                                color: #00ee00;

                            }
                        </style>
                        <table class="table table-bordered table-condensed table-hover">
                            <thead>
                            <tr>
                                <th>عنوان</th>
                                <th>محدودیت دانلود در روز</th>
                                <th>قیمت</th>
                                <th>مدت زمان اشتراک (روز)</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            @if($plans && count($plans))
                            @foreach($plans as $plan)
                                <tr>
                                    <td>{{ $plan->plan_title }}</td>
                                    <td>{{ $plan->	plan_limit_download_count }}</td>
                                    <td>{{ $plan->	plan_price }}</td>
                                    <td>{{ $plan->	plan_days_count }}</td>
                                    <td>
                                        <a class="btn btn-success  btn-block" style="text-decoration: none" href="{{route('frontend.subscribe.index',$plan->plan_id)}}">خرید</a>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                        {{--<select name="plan" id="plan" class="form-control">--}}
                            {{--@if($plans && count($plans))--}}
                                {{--@foreach($plans as $plan)--}}
                                    {{--<option value="{{ $plan->plan_id }}">{{$plan->plan_title}}</option>--}}
                                {{--@endforeach--}}
                            {{--@endif--}}
                        {{--</select>--}}
                    </div>
                </form>




                {{--<p>عنوان {{$fileItem->file_title}}</p>--}}
                {{--<p>عنوان {{$fileItem->file_title}}</p>--}}
                {{--<p>عنوان {{$fileItem->file_title}}</p>--}}


            </div>
        </div>

    </div>

@endsection