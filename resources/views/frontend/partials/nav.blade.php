<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header ">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">فروشگاه لارا</a>
        </div>



        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav">
                @if(!\Illuminate\Support\Facades\Auth::check())
                    <li><a href="{{route('register')}}">ثبت نام <span class="sr-only">(current)</span></a></li>
                    <li><a href="{{route('login')}}">ورود</a></li>

                @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">{{ \Illuminate\Support\Facades\Auth::user()->name.' ' }}به لارافایل خوش آمدید <span class="caret"></span></a>
                    <ul class="dropdown-menu" style="width: 100%">
                        <li><a href="{{ route('user.dashboard') }}">پنل کاربری</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{route('doLogout')}}">خروج</a></li>


                    </ul>
                </li>
                </ul>
            @endif()


        </div>
          <!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>