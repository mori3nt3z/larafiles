@extends('layouts.frontend')
@section('content')

    <div class="col-xs-9 col-md-9">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">فرم ثبت نام </h3>
            </div>
            <div class="panel-body">

                @if(session('registerError'))
                    <div class="alert alert-danger">
                        <p>{{ session('registerError') }}</p>
                    </div>
                @endif()
                <form class="form-horizontal" method="post" action="{{route('doRegister')}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">نام کامل </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="fullName" id="inputEmail3" placeholder="نام کامل خود را وارد کنید... ">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">ایمیل </label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" name="email" id="inputEmail3" placeholder="ایمیل خود را وارد کنید... ">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">کلمه عبور</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="password" id="inputPassword3" placeholder="پسورد خود را وارد کنید...">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success">ثبت نام</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endsection