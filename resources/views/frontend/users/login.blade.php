@extends('layouts.frontend')
@section('content')

    <div class="col-xs-9 col-md-9 ">
        <div class="panel panel-info">
            @if(session('registerSuccess'))
                <div class="alert alert-success">
                    <p>{{ session('registerSuccess') }}</p>
                </div>
            @endif()
            <div class="panel-heading">
                <h3 class="panel-title">فرم ورود کاربر </h3>
            </div>
            <div class="panel-body">
                @if(session('loginError'))
                    <div class="alert alert-danger">
                       <p> {{session('loginError')}}</p>
                    </div>
                    @endif
                <form class="form-horizontal" method="post" action="{{route('doLogin')}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">ایمیل </label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" name="email" id="inputEmail3" placeholder="ایمیل خود را وارد کنید... ">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">کلمه عبور</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="password" id="inputPassword3" placeholder="پسورد خود را وارد کنید...">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember"> من را به خاطر بسپار
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">وزود</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endsection