@extends('layouts.frontend')
@section('content')
    <div class="col-xs-9 col-md-9">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">مشاهده جزییات پکیج  </h3>
            </div>
            <div class="panel-body">

                <div class="list-group">

                        <h4 class="list-group-item-heading ">نام پکیج  :</h4>
                    <br>
                        <p class="list-group-item-text"> {{$packItem->package_title}}</p>

                    <hr>

                        <h4 class="list-group-item-heading "> لیست فایلهای این پکیج  :</h4>

                    <br>
                        <ul class="list-group">
                            @if($packFiles && count($packFiles))
                                @foreach($packFiles as $packFile)

                                   <li class="list-group-item"><a class="list-group-item" href="{{route('frontend.files.details',[$file_id = $packFile->file_id])}}"> {{ $packFile->file_title }} </a></li>

                                @endforeach
                            @endif
                        </ul>

                    <hr>

                        <h4 class="list-group-item-heading "> قیمت  :</h4>
                    <br>
                        <p class="list-group-item-text"> {{$packItem->package_price}}</p>

                    <hr>
                        <h4 class="list-group-item-heading "> تاریخ ثبت :</h4>
                    <br>
                        <p class="list-group-item-text"> {{$packItem->created_at}}</p>

                </div>




             {{--<p>عنوان {{$packItem->package_title}}</p>--}}
             {{--<p>عنوان {{$packItem->package_title}}</p>--}}
             {{--<p>عنوان {{$packItem->package_title}}</p>--}}


            </div>
        </div>

    </div>
    <div class="col-xs-3 col-md-3">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">خرید و دانلود پکیج  </h3>
            </div>


            <div class="panel-body">


            </div>
        </div>
    </div>
@endsection