@extends('layouts.frontend')
@section('content')
    <div class="col-xs-12 col-md-12">
        <div class="panel panel-info">
            @if(session('hasError'))
            <div class="alert alert-danger">
                <p>{{ session('hasError') }}</p>
            </div>
            @endif
            <div class="panel-heading">
                <h3 class="panel-title">خرید اشتراک دانلود فایل </h3>
            </div>
            <div class="panel-body">

                <div class="alert alert-info">
                    <p>
                        اطلاعات خرید طرح اشتراکی
                    </p>
                </div>

                <style>
                    table {
                        background-color: rgb(44, 44, 44);

                    }

                    .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > thead > tr > th {
                        border: 1px solid #0c0c0c;
                        text-align: center;
                    }

                    table tr th {
                        color: rgb(143, 154, 232);

                    }

                    table tr td {
                        color: #00ee00;

                    }
                </style>
                <table class="table table-bordered table-condensed table-hover">
                    <tr>
                        <td>عنوان</td>
                        <td>{{ $plan_item->plan_title }}</td>
                    </tr>
                    <tr>
                        <td>تعداد دانلود روزانه</td>
                        <td>{{ $plan_item->plan_limit_download_count }}</td>
                    </tr>
                    <tr>
                        <td>مدت اشتراک (روز)</td>
                        <td>{{ $plan_item->plan_days_count }}</td>
                    </tr>
                    <tr>
                        <td>قیمت</td>
                        <td>{{ number_format($plan_item->plan_price) }} <span> تومان</span></td>
                    </tr>
                </table>
                {{--{{route('frontend.subscribe.register',$plan_item->plan_id)}}--}}
                <form action="" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <input type="hidden" name="plan_id" value="{{ $plan_item->plan_id }}">
                        <button class="btn btn-success">خرید این طرح</button>
                    </div>
                </form>
            </div>


        </div>
    </div>


@endsection