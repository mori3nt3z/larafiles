@extends('layouts.frontend')
@section('content')
    <div class="col-xs-9 col-md-9">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">مشاهده جزییات فایل </h3>
            </div>
            <div class="panel-body">

                <div class="list-group">
                    <a href="#" class="list-group-item ">
                        <h4 class="list-group-item-heading ">نام فایل :</h4>
                        <hr>
                        <p class="list-group-item-text"> {{$fileItem->file_title}}</p>
                    </a>
                    <a href="#" class="list-group-item ">
                        <h4 class="list-group-item-heading "> توضیحات فایل :</h4>
                        <hr>
                        <p class="list-group-item-text"> {{$fileItem->file_description}}</p>
                    </a>
                    <a href="#" class="list-group-item ">
                        <h4 class="list-group-item-heading "> نوع فایل :</h4>
                        <hr>
                        <p class="list-group-item-text"> {{$fileItem->File_Type_Text}}</p>
                    </a>
                    <a href="#" class="list-group-item ">
                        <h4 class="list-group-item-heading "> تاریخ ثبت :</h4>
                        <hr>
                        <p class="list-group-item-text"> {{$fileItem->created_at}}</p>
                    </a>
                </div>




             {{--<p>عنوان {{$fileItem->file_title}}</p>--}}
             {{--<p>عنوان {{$fileItem->file_title}}</p>--}}
             {{--<p>عنوان {{$fileItem->file_title}}</p>--}}


            </div>
        </div>

    </div>
    <div class="col-xs-3 col-md-3">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">خرید فایل </h3>
            </div>
            @if(session('fileError'))
                <div class="alert alert-danger">
                    {{session('fileError')}}
                </div>
                @endif
            <div class="panel-body">

                @if(\App\utility\User::is_user_subscribed($current_user))
                    <a class="btn btn-primary btn-lg btn-block" style="text-decoration: none"
                       href="{{ route('frontend.files.download',[$fileItem->file_id]) }}">
                        دانلود فایل
                    </a>
                    <a data-fid="{{ $fileItem->file_id }}" class="btn btn-warning btn-lg btn-block btn-report-file" href="">گزارش خطا</a>


                @else
                    <a style="text-decoration: none" href="{{ route('frontend.plans.index') }}">
                        <button class="btn btn-success btn-lg btn-block" style="font-size: 12px;">خرید طرح اشتراکی برای دانلود فایل</button>
                    </a>
                @endif
            </div>
        </div>
    </div>
@endsection