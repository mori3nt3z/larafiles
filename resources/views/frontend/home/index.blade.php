@extends('layouts.frontend')
@section('content')
<div class="col-xs-9 col-md-9">
    <div class="panel panel-info">
        <div class="panel-heading">
            @if(session('loginSuccess'))
                <div class="alert alert-success">
                <p>{{ session('loginSuccess') }}</p>
                </div>
            @endif()
            @if(session('logoutSuccess'))
                <div class="alert alert-success">
                    <p>{{ session('logoutSuccess') }}</p>
                </div>
            @endif()

            <h3 class="panel-title">آخرین فایل های سیستم </h3>
        </div>
        <div class="panel-body">


            @if($files && count($files)>0)
                <ul class="list-group">
                @foreach($files as $file)
                    <li class="list-group-item"><a style="text-decoration: none;" href="{{route('frontend.files.details',[$file->file_id])}}">{{ $file->file_title }}</a></li>
                 @endforeach
                </ul>
            @endif

        </div>
    </div>
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">آخرین پکیج ها  </h3>
        </div>
        <div class="panel-body">


            @if($packages && count($packages)>0)
                <ul class="list-group">
                    @foreach($packages as $package)
                        <li class="list-group-item"><a href="{{ route('frontend.packages.details',[$package->package_id]) }}">{{ $package->package_title }}</a></li>
                    @endforeach
                </ul>
            @endif

        </div>
    </div>
</div>
<div class="col-xs-3 col-md-3"></div>
@endsection()