<div class="row">
    @include('admin.partials.errors')
    <div class="col-xs-12 col-md-6">

        <form action="" method="post" >
            {{ csrf_field() }}
            <div class="form-group">
                <label style="font-size: 14px;" for="package_title"> عنوان دسته بندی :</label>

                <input type="text" class="form-control" name="category_name" id="category_name" value="{{ old('category_name',isset($categoryItem)? $categoryItem->category_name : '') }}">
            </div>



            <div class="form-group">

                <button type="submit" class="btn btn-success">ذخیره اطلاعات</button>
            </div>
        </form>
    </div>
</div>