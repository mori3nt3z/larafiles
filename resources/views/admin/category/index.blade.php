@extends('layouts.admin')
@section('content')
    @include('admin.category.notification')
    <table  class="table table-bordered table-hover">
        <thead style="background-color: #d9edf7;color: #176f6f;">
        @include('admin.category.columns')
        </thead>
    @if($categories && count($categories)>0)



            @foreach($categories as $category)
                @include('admin.category.item',$category)
            @endforeach
        @else
        @include('admin.category.no-item')

    @endif

    </table>
@endsection()