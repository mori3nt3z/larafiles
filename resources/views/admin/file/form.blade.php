<div class="row">
    @include('admin.partials.errors')
    <div class="col-xs-12 col-md-6">

        <form action="" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label style="font-size: 14px;" for="file_title"> عنوان فایل :</label>

                <input type="text" class="form-control" name="file_title" id="file_title" value="{{ old('file_title',isset($fileItem)? $fileItem->file_title : '') }}">
            </div>
            <div class="form-group">
                <label style="font-size: 14px;" for="file_description">توضیحات فایل</label>
                <textarea class="form-control" name="file_description" id="file_description" cols="30" rows="10">
                    {{ old('file_description',isset($fileItem)? $fileItem->file_description : '') }}
                </textarea>
            </div>
            <div class="form-group">
                <label style="font-size: 14px;" for="file_title"> آپلود فایل :</label>

                <input type="file" name="file_item"  >
            </div>

            <div class="form-group">

                <button type="submit" class="btn btn-success">ذخیره اطلاعات</button>
            </div>
        </form>
    </div>
</div>