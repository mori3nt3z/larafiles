@extends('layouts.admin')
@section('content')
    @if($files && count($files)>0)
        <table  class="table table-bordered table-hover">
            <thead style="background-color: #d9edf7;color: #176f6f;">
            @include('admin.file.columns')
            </thead>
            @foreach($files as $file)
                @include('admin.file.item',$file)
            @endforeach
        </table>
    @endif

@endsection()