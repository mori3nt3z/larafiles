<style>
    a{
     text-decoration: none;
        color: white;
        padding: 5px;
    }
    .hover-edit:hover{
      color: #2aff1c;

    }
    .hover-delete:hover{
      color: #ed2e2c;
    }

    .hover-see:hover{
        color: #98cbe8;
    }


</style>
<a title="ویرایش کاربر" href="{{route('admin.users.edit',$user->id)}}" ><i class="glyphicon glyphicon-edit hover-edit "></i></a>
<a title="حذف کاربر" href="{{route('admin.users.delete',$user->id)}}" ><i class="glyphicon glyphicon-trash hover-delete  "></i></a>
<a title="مشاهده پکیج های خریداری شده" href="{{route('admin.users.package',$user->id)}}" ><i class="glyphicon glyphicon-list hover-see  "></i></a>