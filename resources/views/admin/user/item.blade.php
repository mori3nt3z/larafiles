<style>
    .center{
        text-align: center;
        color: #ffffffb8;
    }
</style>
<tr class="center" style=" background-color: #000000bf;">
    <td style="text-align: right">{{ $user->id }}</td>
    <td>{{ $user->name }}</td>
    <td>{{ $user->email }}</td>
    <td>{{ $user->wallet }}</td>
    <td>{{ $user->packages()->count() }}</td>
    <td >
        @include('admin.user.operations',$user)
    </td>
</tr>