@extends('layouts.admin')
@section('content')
    @include('admin.user.notification')
    @if($users && count($users)>0)
        <table  class="table table-bordered table-hover">
            <thead style="background-color: #d9edf7;color: #176f6f;">
            <tr >
                <th style="text-align: center" scope="col">شناسه کاربری</th>
                <th style="text-align: center" scope="col">نام و نام خانوادگی</th>
                <th style="text-align: center" scope="col">ایمیل</th>
                <th style="text-align: center" scope="col">موجودی</th>
                <th style="text-align: center" scope="col">تعداد پکیج های خریداری شده</th>
                <th style="text-align: center" scope="col">عملیات</th>
            </tr>
            </thead>
            @foreach($users as $user)
                @include('admin.user.item',$user)
            @endforeach
        </table>
    @endif
@endsection