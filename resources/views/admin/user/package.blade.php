@extends('layouts.admin')
@section('content')
    <table class="table table-bordered table-hover">

        <thead>
        <tr>
            <td>پکیج</td>
            <td>مبلغ پرداخت شده</td>
            <td>تاریخ پرداخت</td>
        </tr>
        </thead>

        @if($user_package && count($user_package) > 0)

            @foreach($user_package as $package)

                <tr style="text-align: center">

                    <td>{{ $package->package_title }}</td>
                    <td>{{ $package->pivot->amount }}</td>
                    <td>{{ $package->pivot->created_at }}</td>
                </tr>
            @endforeach
            @else
            <tr>
                <td colspan="3">پکیجی یافت نشد </td>
            </tr>
        @endif

    </table>


@endsection