@extends('layouts.admin')
@section('content')
    <table  class="table table-bordered table-hover">
        @include('admin.payment.notification')

            <thead style="background-color: #d9edf7;color: #176f6f;">
            @include('admin.payment.columns')
            </thead>
        @if($payments && count($payments)>0)

            @foreach($payments as $payment)
                @include('admin.payment.item',$payment)
            @endforeach
    </table>
    @endif

@endsection()