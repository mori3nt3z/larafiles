@extends('layouts.admin')
@section('content')
    @if($plans && count($plans)>0)
        @include('admin.plan.notification')
        <table  class="table table-bordered table-hover">
            <thead style="background-color: #d9edf7;color: #176f6f;">
            @include('admin.plan.columns')
            </thead>
            @foreach($plans as $plan)
                @include('admin.plan.item',$plan)
            @endforeach
        </table>
    @endif

@endsection()