<div class="row">
    @include('admin.partials.errors')
    <div class="col-xs-12 col-md-6">

        <form action="" method="post" >
            {{ csrf_field() }}
            <div class="form-group">
                <label style="font-size: 14px;" for="package_title"> عنوان پکیج :</label>

                <input type="text" class="form-control" name="package_title" id="package_title" value="{{ old('package_title',isset($packageItem)? $packageItem->package_title : '') }}">
            </div>
            <div class="form-group">
                <label style="font-size: 14px;" for="package_price">قیمت پکیج :</label>
                <input type="text" class="form-control" name="package_price" id="package_price" value="{{  old('package_price',isset($packageItem)? $packageItem->package_price : '')}}">


            </div>
            <div class="form-group">
                <label style="font-size: 14px;" for="package_price">دسته بندی ها :</label>
                <select name="categorize[]" id="categorize" class="select2 form-control" multiple>

                    @if($categories and count($categories)>0)

                        @foreach($categories as $cat)
                            <option value="{{ $cat->category_id }}" {{(isset($package_categories) && in_array($cat->category_id,$package_categories)) ? 'selected' : ''}}>{{$cat->category_name}}</option>
                        @endforeach
                    @endif

                </select>
            </div>


            <div class="form-group">

                <button type="submit" class="btn btn-success">ذخیره اطلاعات</button>
            </div>
        </form>
    </div>
</div>
