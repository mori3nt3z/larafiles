@extends('layouts.admin')
@section('content')

    @if($files && count($files) >0 )
        <form action="" method="post">
            {{ csrf_field() }}
        <ul class="list-group form-group">
        @foreach($files as $file)
           <li class="list-group-item" >
               <input type="checkbox"  name="files[]" value="{{ $file->file_id }}" {{(isset($package_files) && in_array($file->file_id,$package_files))? 'checked' : ''}} >
               {{ $file->file_title }}
           </li>
            @endforeach
        </ul>
            <div class="list-group">
                <input type="submit" class="btn btn-success" name="submit_package_files" value="ذخیره فایلهای انتخاب شده">
            </div>
        </form>
    @endif
    @endsection