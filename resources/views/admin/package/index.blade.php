@extends('layouts.admin')
@section('content')
    @include('admin.package.notification')
    <table  class="table table-bordered table-hover">
        <thead style="background-color: #d9edf7;color: #176f6f;">
        @include('admin.package.columns')
        </thead>
    @if($packages && count($packages)>0)



            @foreach($packages as $package)
                @include('admin.package.item',$package)
            @endforeach
        @else
        @include('admin.package.no-item')

    @endif

    </table>
@endsection()