<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="/icons/faveicon/laravel2.png" type="image/png" >
    <link href="/css/bootstrap.min.css" rel="stylesheet" >


    <link href="/css/app.css" rel="stylesheet" >

    <title>Lara Files admin panel</title>
</head>
<body>

@include('admin.partials.nav')

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ isset($panel_title)? $panel_title : '' }}</h3>
                </div>
                <div class="panel-body">
                     @yield('content')
                </div>
            </div>

        </div>
    </div>
</div>

<script  src="/js/jquery.min.js"></script>
<script  src="/js/bootstrap.min.js"></script>
<script  src="/js/app.js"></script>
<script  src="/js/custom-admin.js"></script>

</body>
</html>